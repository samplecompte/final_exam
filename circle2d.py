#Sam LeCompte
#1/25/16
#Block 2
import math

class Circle2D:
    def __init__(self,x,y,radius):
        self.__x = x
        self.__y = y
        self.__radius = radius

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_radius(self):
        return self.__radius

    def getArea(self):
        return math.pi * self.get_radius()**2

    def getPerimeter(self):
        return math.pi * self.get_radius()*2







